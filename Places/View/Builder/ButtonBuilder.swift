//
//  ButtonBuilder.swift
//  Places
//
//  Created by Amir on 11.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class ButtonBuilder {
    
    private var button: UIButton!
    
    init() {
        button = UIButton(type: .custom)
    }
    
    func roundCorners() -> Self {
        
        button.layer.cornerRadius = 100
        return self
    }
    
    func cornerRadius(_ radius: CGFloat) -> Self {
        
        button.layer.cornerRadius = radius
        return self
    }
    
    func image(_ image: UIImage) -> Self {
                
        button.setImage(image, for: .normal)
        return self
    }
    
    func title(_ text: String) -> Self {
        
        button.setTitle(text, for: .normal)
        return self
    }
    
    func titleColor(_ color: UIColor) -> Self {
        
        button.setTitleColor(color, for: .normal)
        return self
    }
    
    func font(_ font: UIFont) -> Self {
        button.titleLabel?.font = font
        return self
    }
    
    func backgroundColor(_ color: UIColor) -> Self {
        
        button.backgroundColor = color
        return self
    }
    
    func build() -> UIButton {
        return button
    }
}
