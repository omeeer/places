//
//  DescriptionTableView.swift
//  Places
//
//  Created by Amir on 12.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class DescriptionTableView: UIView {

    private let titleLabel = UILabel(textColor: .white, font: .heavyFont(size: 18)).enableConstraints()
    private let tableView = UITableView().enableConstraints()
    private lazy var placeInfoIcons = [#imageLiteral(resourceName: "placeLocation.pdf"), #imageLiteral(resourceName: "placePhone.pdf"), #imageLiteral(resourceName: "placeSite.pdf")]
    
    var placeDTO: PlaceDTO! {
        didSet {
            mainInfo.append(placeDTO.address)
            mainInfo.append(placeDTO.phone)
            mainInfo.append(placeDTO.site)
            additionalInfo.append(AdditionalInfo(type: .category, text: placeDTO.type))
            additionalInfo.append(AdditionalInfo(type: .workingTime, text: placeDTO.workTime))
        }
    }
    
    struct AdditionalInfo {
        
        let type: DescriptionCellType
        let text: String?
    }
    
    var mainInfo = [String]()
    var additionalInfo = [AdditionalInfo]()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTitleLabel()
        setupTableView()
    }
    
    func setupTitleLabel() {
        
        addSubview(titleLabel)
        titleLabel.sizeToFit()
        titleLabel.text = "Описание"
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20)
        ])
    }
    
    func setupTableView() {
        
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
        [UITableViewCell.self, DescriptionCell.self].forEach { tableView.register(cell: $0) }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorColor = .white
        tableView.estimatedRowHeight = 70
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor, constant: -15),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DescriptionTableView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
        switch indexPath.row {
        case 0,1,2:
            let cell = tableView.dequeueTypedCell(for: indexPath)
                        
            cell.imageView?.image = placeInfoIcons[indexPath.row]
            cell.textLabel?.text = mainInfo[indexPath.row]
            cell.textLabel?.numberOfLines = 0
            cell.selectionStyle = .none
            cell.textLabel?.font = .bookFont(size: 16)
            cell.backgroundColor = .clear
            
            return cell
        default:            
            let cell: DescriptionCell = tableView.dequeueTypedCell(for: indexPath)
            
            cell.cellType = additionalInfo[indexPath.row - 3].type
            cell.valueLabel.text = additionalInfo[indexPath.row - 3].text
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0,1,2:
            
            if let url = URL(string: mainInfo[indexPath.row]) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
            if let url = URL(string: "tel://" + mainInfo[indexPath.row]) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
        default:
            return
        }
    }
    
}
