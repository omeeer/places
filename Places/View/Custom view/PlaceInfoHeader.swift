//
//  PlaceInfoHeader.swift
//  Places
//
//  Created by Amir on 25.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class PlaceInfoHeader: UIView {
    
    private let nameLabel = UILabel(textColor: .white, font: .heavyFont(size: 24))
    private let typeLabel = UILabel(textColor: .white, font: .bookFont(size: 14))
    private let addressLabel = UILabel(textColor: .white, font: .bookFont(size: 16))
    
    private var placeImageView: UIImageView!
    
    private lazy var labelsStack = UIStackView(views: [nameLabel, typeLabel, addressLabel], axis: .vertical, spacing: 8, distribution: .fill).enableConstraints()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    func configure(with place: PlaceDTO) {
        placeImageView.image = #imageLiteral(resourceName: "example")
        nameLabel.text = place.name
        typeLabel.text = place.type
        addressLabel.text = place.address
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initViews() {
        
        placeImageView = UIImageView().enableConstraints()
        placeImageView.contentMode = .scaleAspectFill
                
        nameLabel.sizeToFit()
        nameLabel.textAlignment = .center
        nameLabel.numberOfLines = 0
        
        typeLabel.sizeToFit()
        typeLabel.textAlignment = .center
        typeLabel.numberOfLines = 0
        
        addressLabel.sizeToFit()
        addressLabel.textAlignment = .center
        addressLabel.numberOfLines = 0
        
        insertSubview(placeImageView, at: 0)
        
        labelsStack.alignment = .center
        addSubview(labelsStack)
        
        NSLayoutConstraint.activate([
            placeImageView.widthAnchor.constraint(equalTo: widthAnchor),
            placeImageView.heightAnchor.constraint(equalTo: heightAnchor),
            labelsStack.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            labelsStack.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 10),
            labelsStack.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let gradientMaskLayer = CAGradientLayer()
        gradientMaskLayer.frame = placeImageView.bounds
        let color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4)
        gradientMaskLayer.colors = [color.cgColor, UIColor.white.cgColor]
        gradientMaskLayer.locations = [0, 1]
        placeImageView.layer.mask = gradientMaskLayer
    }
    
}
