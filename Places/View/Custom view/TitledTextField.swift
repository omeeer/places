//
//  TitledTextField.swift
//  Places
//
//  Created by Amir on 01.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class TitledTextField: UIView {
    
    private let textField = UITextField()
    private let titleLabel = UILabel(textColor: .white, font: .heavyFont(size: 18))
    private let bottomLineView = UIView()
    private lazy var mainStackView = UIStackView(views: [titleLabel, textField, bottomLineView], axis: .vertical, spacing: 10, distribution: .fill).enableConstraints()
    
    init(title: String, placeholder: String) {
        super.init(frame: .zero)
        
        initViews()
        
        titleLabel.text = title
        textField.placeholder = placeholder
    }
    
    func getTextFieldText() -> String? {
        return textField.text
    }
    
    private func initViews() {
        
        textField.font = .romanFont(size: 13)
        textField.keyboardType = .numberPad
        textField.addDoneButtonOnKeyboard(with: "Готово")
        textField.delegate = self
        textField.textColor = #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8901960784, alpha: 1)
        
        bottomLineView.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8901960784, alpha: 1)
        mainStackView.setCustomSpacing(5, after: textField)
        
        mainStackView.alignment = .leading
        addSubview(mainStackView)
        
        NSLayoutConstraint.activate([
            
            
            bottomLineView.heightAnchor.constraint(equalToConstant: 1),
            bottomLineView.widthAnchor.constraint(equalTo: mainStackView.widthAnchor),
            textField.widthAnchor.constraint(equalTo: mainStackView.widthAnchor),
            
            mainStackView.topAnchor.constraint(equalTo: topAnchor),
            mainStackView.rightAnchor.constraint(equalTo: rightAnchor),
            mainStackView.leftAnchor.constraint(equalTo: leftAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TitledTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()        
        return true
    }
    
}
