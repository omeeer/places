//
//  MapOverlay.swift
//  Places
//
//  Created by Amir on 26.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol MapOverlayDelegate: AnyObject {
    func didPressOnOverlay(with place: PlaceDTO)
}

class MapOverlay: UIView {
    
    private let placeNameLabel = UILabel(textColor: .white, font: .heavyFont(size: 15))
    private let bottomLabel = UILabel(textColor: .lightGrayColor, font: .lightFont(size: 10))
    private let separatorView = UIView().enableConstraints()
    private let arrowView = UIImageView(image: #imageLiteral(resourceName: "disclosure"))
    
    weak var delegate: MapOverlayDelegate?
    
    private var place: PlaceDTO?
    
    private lazy var labelStack = UIStackView(views: [placeNameLabel, bottomLabel], axis: .vertical, spacing: 0, distribution: .fill)
    
    private lazy var mainStack = UIStackView(views: [labelStack, separatorView, arrowView], axis: .horizontal, spacing: 42, distribution: .fill).enableConstraints()
    
    private let arrowViewSize = CGSize(width: 11, height: 10)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    convenience init(place: PlaceDTO) {
        self.init()
                
        self.place = place
        placeNameLabel.text = place.name
        bottomLabel.text = "\(place.type) • \(place.distance) м"
    }
    
    private func initViews() {
        
        layer.cornerRadius = 10
        layer.masksToBounds = true
        backgroundColor = .darkBgColor
        enableConstraints()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressOnMapOverlay))
        addGestureRecognizer(tapGesture)
        
        separatorView.backgroundColor = .lightGrayColor
        
        arrowView.contentMode = .scaleAspectFit
        
        [placeNameLabel, bottomLabel].forEach {
            $0.sizeToFit()
            $0.numberOfLines = 0
        }
                
        mainStack.alignment = .center
        mainStack.setCustomSpacing(7, after: separatorView)
        addSubview(mainStack)
        
        NSLayoutConstraint.activate([
            
            arrowView.heightAnchor.constraint(equalToConstant: arrowViewSize.height),
            arrowView.widthAnchor.constraint(equalToConstant: arrowViewSize.width),

            separatorView.widthAnchor.constraint(equalToConstant: 1),
            separatorView.heightAnchor.constraint(equalTo: mainStack.heightAnchor),
            
            mainStack.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            mainStack.leftAnchor.constraint(equalTo: leftAnchor, constant: 6),
            mainStack.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            mainStack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
    
    @objc private func didPressOnMapOverlay(_ touch: UITapGestureRecognizer) {
        
        guard let selectedPlace = place else { return }
        
        delegate?.didPressOnOverlay(with: selectedPlace)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
