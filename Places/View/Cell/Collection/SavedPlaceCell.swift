//
//  SavedPlaceCell.swift
//  Places
//
//  Created by Amir on 24.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol SavedPlaceCellDelegate: AnyObject {
    func bookmarkButtonPressed(with place: PlaceDTO)
}

class SavedPlaceCell: UICollectionViewCell {
   
    private let placeImageView = UIImageView(roundCorners: true, cornerRadius: 10).enableConstraints()
    private let placeNameLabel = UILabel(textColor: .white, font: .blackFont(size: 20)).enableConstraints()
    private let bookmarkButton = UIButton().enableConstraints()
    
    private weak var delegate: SavedPlaceCellDelegate?
    private var place: PlaceDTO?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
        initConstraints()
    }
    
    func configure(with place: PlaceDTO, delegate: SavedPlaceCellDelegate) {
        
        placeImageView.image = UIImage(named: place.image)
        placeNameLabel.text = place.name
        self.delegate = delegate
        self.place = place
    }
    
    private func initViews() {
        
        placeImageView.contentMode = .scaleAspectFill
        placeNameLabel.sizeToFit()
        placeNameLabel.numberOfLines = 0
        bookmarkButton.setImage(#imageLiteral(resourceName: "bookmarkFilled"), for: .normal)
        bookmarkButton.addTarget(self, action: #selector(bookmarkButtonPressed), for: .touchUpInside)
        
        contentView.addSubview(placeImageView)
        contentView.addSubview(bookmarkButton)
        contentView.addSubview(placeNameLabel)
    }
    
    private func initConstraints() {
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
                         
            contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width / 2 - 30),
            
            placeImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            placeImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            placeImageView.heightAnchor.constraint(equalTo: contentView.widthAnchor),
            
            bookmarkButton.topAnchor.constraint(equalTo: placeImageView.topAnchor, constant: 10),
            bookmarkButton.rightAnchor.constraint(equalTo: placeImageView.rightAnchor, constant: -10),
            bookmarkButton.widthAnchor.constraint(equalToConstant: 30),
            bookmarkButton.heightAnchor.constraint(equalToConstant: 30),
                        
            placeNameLabel.topAnchor.constraint(equalTo: placeImageView.bottomAnchor, constant: 5),
            placeNameLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: placeNameLabel.bottomAnchor)
        ])
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return contentView.systemLayoutSizeFitting(CGSize(width: UIScreen.main.bounds.width / 2 - 30, height: 1))
    }
    
    @objc private func bookmarkButtonPressed() {
        
        guard let place = place else { return }
        
        delegate?.bookmarkButtonPressed(with: place)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
