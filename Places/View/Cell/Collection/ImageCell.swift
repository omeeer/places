//
//  ImageCell.swift
//  Places
//
//  Created by Amir on 11.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class PlacePhotoCell: UICollectionViewCell {
    
    private let placeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    func configure(with image: UIImage?) {
        placeImageView.image = image != nil ? image : #imageLiteral(resourceName: "photograph")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(placeImageView)
        backgroundColor = .darkBgColor
        NSLayoutConstraint.activate([
        
            placeImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            placeImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            placeImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            placeImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
