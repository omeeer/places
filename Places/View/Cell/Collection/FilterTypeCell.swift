//
//  FilterTypeCell.swift
//  Places
//
//  Created by Amir on 24.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class FilterTypeCell: UICollectionViewCell {
    
    private let imageView = UIImageView(roundCorners: true, cornerRadius: 10).enableConstraints()
    private let typeLabel = UILabel(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7), font: .lightFont(size: 12)).enableConstraints()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
        initConstraints()
    }
    
    private func initViews() {
        imageView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7)
    }
    
    var isActive: Bool = true {
        didSet {
            isActive ? setDefaultState() : setActiveState()
        }
    }
    
    private func setDefaultState() {
        typeLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7)
        imageView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7)
    }
    
    private func setActiveState() {
        typeLabel.textColor = .appOrange
        imageView.tintColor = .appOrange
    }
    
    func configure(with placeType: PlaceType) {
        imageView.image = placeType.icon.withRenderingMode(.alwaysTemplate)        
        typeLabel.text = placeType.title
    }
    
    private func initConstraints() {
        
        let stack = UIStackView(arrangedSubviews: [imageView, typeLabel]).enableConstraints()
        stack.axis = .vertical
        stack.spacing = 0
        stack.alignment = .center
        
        addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: topAnchor),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor),
            stack.leftAnchor.constraint(equalTo: leftAnchor),
            stack.rightAnchor.constraint(equalTo: rightAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
