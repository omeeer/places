//
//  PlaceCell.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {
    
    private let placeImageView = UIImageView(roundCorners: true, cornerRadius: 8).enableConstraints()
    private let detailArrowImageView = UIImageView(image: #imageLiteral(resourceName: "detailArrow"))
    private let nameLabel = UILabel(textColor: .white, font: .mediumFont(size: 18))
    private let typeLabel = UILabel(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5), font: .mediumFont(size: 12))
    private let distanceLabel = UILabel(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6), font: .mediumFont(size: 12))
    private lazy var placeInfoLabelsStack = UIStackView(views: [nameLabel, typeLabel, distanceLabel], axis: .vertical, spacing: 0, distribution: .equalSpacing).enableConstraints()
    
    private let distanceImageView = UIImageView(image: #imageLiteral(resourceName: "distanceIcon.pdf"))
        
    private let arrowImageViewSize = CGSize(width: 9, height: 15)
    private let placeImageSize = CGSize(width: 56, height: 61)
    private let labelHeight: CGFloat = 20
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initConstraints()
    }
    
    private func initViews() {
    
        backgroundColor = .clear
        
        nameLabel.numberOfLines = 0
        nameLabel.sizeToFit()
        distanceImageView.contentMode = .scaleAspectFit
    }
    
    private func initConstraints() {
        
        initViews()
        
        let mainStackView = UIStackView(views: [placeImageView, placeInfoLabelsStack, detailArrowImageView], axis: .horizontal, spacing: 10, distribution: .fillProportionally).enableConstraints()
        mainStackView.alignment = .center
        
        addSubview(mainStackView)
        
        NSLayoutConstraint.activate([
            
            mainStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            mainStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            mainStackView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            
            detailArrowImageView.widthAnchor.constraint(equalToConstant: 9),
            detailArrowImageView.heightAnchor.constraint(equalToConstant: 15),
            placeImageView.widthAnchor.constraint(equalToConstant: placeImageSize.width),
            placeImageView.heightAnchor.constraint(equalToConstant: placeImageSize.height)
        ])
    }
    
    func configure(with place: PlaceDTO) {
        
        placeImageView.image = UIImage(named: place.image)
        nameLabel.text = place.name
        typeLabel.text = place.type
        
        var destinationText = ""
        let distance = place.distance
        let distanceText = distance >= 1000 ? "\(distance / 1000) км" : "\(distance) м"
        destinationText = "Расстояние: " + distanceText
        
        distanceLabel.text = destinationText
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
