//
//  DescriptionCell.swift
//  Places
//
//  Created by Amir on 13.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

enum DescriptionCellType {
    case category, workingTime
}

class DescriptionCell: UITableViewCell {
    
    private let typeLabel = UILabel(textColor: .white, font: .heavyFont(size: 16)).enableConstraints()
    let valueLabel = UILabel(textColor: #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8901960784, alpha: 1), font: .romanFont(size: 13)).enableConstraints()
    
    public var cellType: DescriptionCellType = .category {
        didSet {
            switch cellType {
            case .category:
                typeLabel.text = "Категория"
            case .workingTime:
                typeLabel.text = "Часы работы"
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initViews()
    }
    
    private func initViews() {
        
        selectionStyle = .none
        
        typeLabel.sizeToFit()
        valueLabel.sizeToFit()
        
        backgroundColor = .clear

        let stack = UIStackView(arrangedSubviews: [typeLabel, valueLabel]).enableConstraints()
        stack.axis = .vertical
        stack.spacing = 12
        stack.distribution = .fillEqually
        addSubview(stack)
                
        NSLayoutConstraint.activate([

            stack.leftAnchor.constraint(equalTo: leftAnchor, constant: 50),
            stack.topAnchor.constraint(equalTo: topAnchor, constant: 17),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -9)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
