//
//  NotificationName+Extension.swift
//  Places
//
//  Created by Amir on 31.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let didReceiveCategories = Notification.Name("didReceiveCategories")
    static let didReceiveRoute = Notification.Name("didReceiveRoute")
}
