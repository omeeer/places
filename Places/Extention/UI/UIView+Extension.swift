//
//  UIView.swift
//  Places
//
//  Created by Amir on 24.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        layer.mask = mask
    }
    
    @discardableResult
    func enableConstraints() -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        return self
    }
    
}
