//
//  UIStackView+Extension.swift
//  Places
//
//  Created by Amir on 25.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIStackView {

    convenience init(views: [UIView], axis: NSLayoutConstraint.Axis, spacing: CGFloat, distribution: UIStackView.Distribution) {
        self.init(arrangedSubviews: views)
        
        self.axis = axis
        self.spacing = spacing
        self.distribution = distribution
    }

}
