//
//  UITableView+Extensions.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    
    func dequeueTypedCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
    
    func register(cell: UITableViewCell.Type) {
        self.register(cell, forCellReuseIdentifier: cell.identifier)
    }
    
}
