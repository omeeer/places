//
//  UIFont.swift
//  Places
//
//  Created by Amir on 01.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIFont {
    
    static func blackFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Black", size: size)!
    }
   
    static func mediumFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Medium", size: size)!
    }
    
    static func standartFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir", size: size)!
    }
    
    static func bookFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Book", size: size)!
    }
    
    static func heavyFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Heavy", size: size)!
    }
    
    static func romanFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Roman", size: size)!
    }
    static func lightFont(size: CGFloat) -> UIFont {
        return UIFont(name: "Avenir-Light", size: size)!
    }
}


