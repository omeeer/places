//
//  UIColor.swift
//  Places
//
//  Created by Amir on 01.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let lightBgColor = #colorLiteral(red: 0.1411764706, green: 0.1490196078, blue: 0.2, alpha: 1)
    static let darkBgColor = #colorLiteral(red: 0.05490196078, green: 0.05882352941, blue: 0.09803921569, alpha: 1)
    static let appOrange = #colorLiteral(red: 0.9176470588, green: 0.5098039216, blue: 0.2235294118, alpha: 1)
    static let lightGrayColor = #colorLiteral(red: 0.662745098, green: 0.6509803922, blue: 0.6509803922, alpha: 1)
}
