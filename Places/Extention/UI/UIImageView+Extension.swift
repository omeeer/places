//
//  UIImageView+Extension.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIImageView {
    
    convenience init(roundCorners: Bool = false, cornerRadius: CGFloat = 0) {
        self.init()
        
        contentMode = .scaleAspectFit
        
        guard roundCorners else { return }
        
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
    }
}

