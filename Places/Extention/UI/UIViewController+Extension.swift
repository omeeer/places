//
//  UIViewController+Extension.swift
//  Places
//
//  Created by Amir on 26.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func addCloseButton() {

        let closeButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(closeButtonPressed))
        navigationItem.leftBarButtonItem  = closeButton
    }

   @objc private func closeButtonPressed() {
        dismiss(animated: true)
    }
    
    public func changeRoot(vc: UIViewController) {
        
        guard let window = UIApplication.shared.windows.first else {return }
        
        window.rootViewController = vc
                
        UIView.transition(with: window, duration: 0.3, options: .curveEaseIn, animations: {})
    }
}
