//
//  UILabel+Extension.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

extension UILabel {
    
    convenience init(textColor: UIColor, font: UIFont) {
        self.init()
        
        sizeToFit()
        self.textColor = textColor
        self.font = font
    }
    
}
