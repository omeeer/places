//
//  UICollectionView+Extension.swift
//  Places
//
//  Created by Amir on 07.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit


extension UICollectionViewCell {

    static var identifier: String {
        return String(describing: self)
    }
}

extension UICollectionView {
        
    func dequeueTypedCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as! T
    }
    
    func register(cell: UICollectionViewCell.Type) {
        self.register(cell, forCellWithReuseIdentifier: cell.identifier)
    }
    
}
