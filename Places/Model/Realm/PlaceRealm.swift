//
//  PlaceRealm.swift
//  Places
//
//  Created by Amir on 05.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import RealmSwift

@objcMembers
class PlaceRealm: Object {
    
    dynamic var name: String = String()
    dynamic var type: String = String()
    dynamic var image: String = String()
    dynamic var distance: Double = 0
    dynamic var address: String = String()
    dynamic var phone: String = String()
    dynamic var site: String = String()
    dynamic var workTime: String = String()
    dynamic var coordinates: PositionRealm!
    dynamic var identifier: String = String()
    
    override class func primaryKey() -> String? {
        return #keyPath(PlaceRealm.identifier)
    }
    
    convenience init(name: String = String(), type: String = String(), image: String = String(), distance: Double = 0, address: String = String(), phone: String = String(), site: String = String(), workTime: String = String(), coordinates: PositionRealm = PositionRealm(), identifier: String = String()) {
        self.init()
        self.name = name
        self.type = type
        self.image = image
        self.distance = distance
        self.address = address
        self.phone = phone
        self.site = site
        self.workTime = workTime
        self.coordinates = coordinates
        self.identifier = identifier
    }
    
    func convertToDTO() -> PlaceDTO {
        return PlaceDTO(name: name, type: type, image: image, distance: distance, address: address, phone: phone, site: site, workTime: workTime, coordinates: coordinates.convertToPosition(), identifier: identifier)
    }
}


@objcMembers
class PositionRealm: Object {
    
    dynamic var lat: Double = 0
    dynamic var lng: Double = 0
    
    convenience init(lat: Double, lng: Double) {
        self.init()
        self.lat = lat
        self.lng = lng
    }
    
    func convertToPosition() -> Position {
        return Position(lat: lat, lng: lng)
    }
    
}
