//
//  Place.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

struct PlaceDTO {
    
    let name: String
    let type: String
    let image: String
    let distance: Double
    let address: String
    let phone: String
    let site: String
    let workTime: String
    let coordinates: Position
    let identifier: String
    
    static let mockup = PlaceDTO(name: "Starbucks", type: "Кофейня", image: "starbucks", distance: 200, address: "Пушкина, 4", phone: "+78436665543", site: "www.starbucks.com", workTime: "10:00-18:00", coordinates: Position(lat: 55.751510, lng: 49.197060), identifier: "2321432143")
    
    func toRealmModel() -> PlaceRealm {
        return PlaceRealm(name: name, type: type, image: image, distance: distance, address: address, phone: phone, site: site, workTime: workTime, coordinates: PositionRealm(lat: coordinates.lat, lng: coordinates.lng), identifier: identifier)
    }
}

