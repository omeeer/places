//
//  CategoryType.swift
//  Places
//
//  Created by Amir on 01.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

enum CategoryType: String {
        
    case coffeeHouse = "100-1100-0000" // кофейни
    case clothing = "600-6800-0000" // магазины одежды
    case restaurant = "100-1000-0000" // рестораны
    case bar = "200-2000-0011" // бары
    
    case pharmacy = "600-6400-0070" // аптеки
    case nightClub = "200-2000-0012" // ночные клубы
    case fastFood = "100-1000-0009" // фасфуд
    case barberShop = "600-6950-0399" // барбершоп
        
    case hairSalon = "600-6950-0401" // салон красоты
    case gym = "800-8600-0191" // спорт зал
    case museum = "200-2200-0000" // музеи
    case bookstore = "600-6700-0087" // книжные
    
    case foodStore = "600-6300-0066" // продуктовые
    case cinema = "200-2100-0019" // кинотеатры
    case petGoods = "600-6900-0097" // товары для животных
    case karaoke = "200-2000-0014" // театр
}
