//
//  Position.swift
//  Places
//
//  Created by Amir on 31.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import Foundation

struct Position {

    let lat: Double
    let lng: Double
}
