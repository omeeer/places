//
//  MapRoute.swift
//  Places
//
//  Created by Amir on 06.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import heresdk

struct MapRoute {
    
    let route: Route
    let initialCoordinates: GeoCoordinates
    let destinaionCoordinates: GeoCoordinates
}
