//
//  OnboardingViewController.swift
//  Places
//
//  Created by Amir on 02.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import paper_onboarding

class OnboardingViewController: UIViewController {
    
    private let skipButton = ButtonBuilder()
        .title("Пропустить")
        .font(.heavyFont(size: 18))
        .backgroundColor(.clear)
        .titleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5))
        .build().enableConstraints()
    
    private var onboardingView: PaperOnboarding!
    private var pages = [OnboardingItemInfo]()
    
    private let userDefaultsService = UserDefaultsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initOnboardingData()
        initOnboarding()
    }
    
    private func initOnboardingData() {
        
        let backgroundColor: UIColor = .lightBgColor
        let titleFont: UIFont = .blackFont(size: 21)
        let titleColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let descriptionColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
        let descriptionFont: UIFont = .mediumFont(size: 21)
        
        pages = [
            OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "worldwide"),
                               title: "Находите новые интересные места",
                               description: "прямо на карте или списком с указанием расстояния",
                               pageIcon: UIImage(),
                               color: backgroundColor,
                               titleColor: titleColor,
                               descriptionColor: descriptionColor,
                               titleFont: titleFont,
                               descriptionFont: descriptionFont),
            OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "process"),
                               title: "Изучайте всю информацию о них ",
                               description: "чтобы решить, стоит ли его посещать",
                               pageIcon: UIImage(),
                               color: backgroundColor,
                               titleColor: titleColor,
                               descriptionColor: descriptionColor,
                               titleFont: titleFont,
                               descriptionFont: descriptionFont),
            OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "compass"),
                               title: "Стройте маршрут ",
                               description: "до выбранного места",
                               pageIcon: UIImage(),
                               color: backgroundColor,
                               titleColor: titleColor,
                               descriptionColor: descriptionColor,
                               titleFont: titleFont,
                               descriptionFont: descriptionFont),
            OnboardingItemInfo(informationImage: #imageLiteral(resourceName: "save"),
                               title: "Сохраняйте посещенные или желаемые места в избранное",
                               description: "и держите их всегда под рукой",
                               pageIcon: UIImage(),
                               color: backgroundColor,
                               titleColor: titleColor,
                               descriptionColor: descriptionColor,
                               titleFont: titleFont,
                               descriptionFont: descriptionFont)
        ]
    }
    
    private func initOnboarding() {
        
        onboardingView = PaperOnboarding(pageViewBottomConstant: 30)
        onboardingView.translatesAutoresizingMaskIntoConstraints = false
        onboardingView.dataSource = self
        onboardingView.delegate = self
        view.addSubview(onboardingView)
        
        skipButton.addTarget(self, action: #selector(skipButtonPressed), for: .touchUpInside)
        
        view.addSubview(skipButton)
        
        NSLayoutConstraint.activate([
            
            skipButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 21),
            skipButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            skipButton.heightAnchor.constraint(equalToConstant: 25)
        ])
        
        for attribute: NSLayoutConstraint.Attribute in [.left, .right, .top, .bottom] {
            let constraint = NSLayoutConstraint(item: onboardingView!,
                                                attribute: attribute,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: attribute,
                                                multiplier: 1,
                                                constant: 0)
            view.addConstraint(constraint)
        }
    }
    
    @objc private func skipButtonPressed() {
        
        userDefaultsService.save(value: true, for: Constants.onboardingUserDefaultsKey)
        changeRoot(vc: MainTabViewController())
    }
}

extension OnboardingViewController: PaperOnboardingDataSource, PaperOnboardingDelegate {
    
    func onboardingPageItemColor(at index: Int) -> UIColor {
        return .appOrange
    }
    
    func onboardingItemsCount() -> Int {
        return pages.count
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {        
        return pages[index]
    }
}
