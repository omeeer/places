//
//  FilterViewController.swift
//  Places
//
//  Created by Amir on 17.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

struct PlaceType {
    
    let icon: UIImage
    let title: String
    let category: CategoryType
}

class FilterViewController: AppViewController {
    
    private let cancelButton = ButtonBuilder()
        .title("Отмена")
        .titleColor(.appOrange)
        .font(.mediumFont(size: 18))
        .backgroundColor(.clear)
        .build()
    private let doneButton = ButtonBuilder()
        .title("Готово")
        .titleColor(.appOrange)
        .font(.mediumFont(size: 18))
        .backgroundColor(.clear)
        .build()
    
    private var selectedTypes = [String]()
    
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout()).enableConstraints()
    
    private let placeTypes = [PlaceType(icon: #imageLiteral(resourceName: "coffee"), title: "Кофейня", category: .coffeeHouse),
                              PlaceType(icon: #imageLiteral(resourceName: "clothingStore"), title: "Одежда", category: .clothing),
                              PlaceType(icon: #imageLiteral(resourceName: "cafe"), title: "Кафе", category: .restaurant),
                              PlaceType(icon: #imageLiteral(resourceName: "beer"), title: "Бар", category: .bar),
                              PlaceType(icon: #imageLiteral(resourceName: "medicine"), title: "Аптека", category: .pharmacy),
                              PlaceType(icon: #imageLiteral(resourceName: "club"), title: "Ночной клуб", category: .nightClub),
                              PlaceType(icon: #imageLiteral(resourceName: "fastFood"), title: "Фастфуд", category: .fastFood),
                              PlaceType(icon: #imageLiteral(resourceName: "barbershop"), title: "Барбершоп", category: .barberShop),
                              PlaceType(icon: #imageLiteral(resourceName: "hairdryer"), title: "Салон красоты", category: .hairSalon),
                              PlaceType(icon: #imageLiteral(resourceName: "dumbbell"), title: "Спорт зал", category: .gym),
                              PlaceType(icon: #imageLiteral(resourceName: "museum"), title: "Музей", category: .museum),
                              PlaceType(icon: #imageLiteral(resourceName: "bookstore"), title: "Книжный", category: .bookstore),
                              PlaceType(icon: #imageLiteral(resourceName: "diet"), title: "Продукты", category: .foodStore),
                              PlaceType(icon: #imageLiteral(resourceName: "pet"), title: "Пет гудс", category: .petGoods),
                              PlaceType(icon: #imageLiteral(resourceName: "popcorn"), title: "Кинотеатр", category: .cinema),
                              PlaceType(icon: #imageLiteral(resourceName: "karaoke"), title: "Караоке", category: .karaoke)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        title = "Фильтр"
        navigationItem.leftBarButtonItem  = UIBarButtonItem(customView: cancelButton)
        navigationItem.rightBarButtonItem  = UIBarButtonItem(customView: doneButton)
        initConstraints()
    }
    
    private func initCollectionView() {
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.register(cell: FilterTypeCell.self)
        view.addSubview(collectionView)
    }
    
    private func initViews() {
        
        initCollectionView()
        
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(doneButtonPressed), for: .touchUpInside)
    }
    
    @objc private func cancelButtonPressed() {
        dismiss(animated: true)
    }
    
    @objc private func doneButtonPressed() {
                
        dismiss(animated: true) {
            NotificationCenter.default.post(name: .didReceiveCategories, object: self.selectedTypes)
        }        
    }
    
    private func initConstraints() {
        
        NSLayoutConstraint.activate([
            
            collectionView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 20),
            collectionView.leftAnchor.constraint(equalTo: view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension FilterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return placeTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: FilterTypeCell = collectionView.dequeueTypedCell(for: indexPath)
        
        cell.configure(with: placeTypes[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width / 4 - 10, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FilterTypeCell
        
        let selectedCategory = placeTypes[indexPath.row].category.rawValue
        
        selectedTypes.contains(selectedCategory) ? selectedTypes.removeAll(where: { $0 == selectedCategory }) : selectedTypes.append(selectedCategory)
        
        cell.isActive.toggle()
    }
}
