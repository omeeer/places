//
//  AppViewController.swift
//  Places
//
//  Created by Amir on 01.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class AppViewController: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .darkBgColor
        
        guard let navController = navigationController else { return }
        navController.navigationBar.prefersLargeTitles = true
        
        navigationItem.backBarButtonItem = UIBarButtonItem()
        
        navController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.heavyFont(size: 17),
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        
        navController.navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.tintColor = .white
                
        navController.navigationBar.isTranslucent = true
        navController.navigationBar.barTintColor = .darkBgColor
        
    }
}

