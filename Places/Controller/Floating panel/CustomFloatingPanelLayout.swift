//
//  CustomFloatingPanelLayout.swift
//  Places
//
//  Created by Amir on 22.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import FloatingPanel

class CustomFloatingPanelLayout: FloatingPanelLayout {
    
    var initialPosition: FloatingPanelPosition {
        .half
    }
    
    func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        
        switch position {
            
        case .tip:
            return 120
        case .half:
            return 300
        default:
            return nil
        }
    }
}
