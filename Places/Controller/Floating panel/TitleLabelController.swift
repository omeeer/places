//
//  TitleLabelController.swift
//  Places
//
//  Created by Amir on 17.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class TitleLabelController: UIViewController {

    public let titleLabel = UILabel(textColor: .white, font: .blackFont(size: 26))
    
    var titleText: String! {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTitleLabel()
        view.backgroundColor = .darkBgColor
    }
    
    private func initTitleLabel() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Рядом"
        view.addSubview(titleLabel)
    }
}
