//
//  BottomSheetController.swift
//  Places
//
//  Created by Amir on 16.03.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol BottomSheetDelegate: AnyObject {
    func searchButtonPressed(with text: String)
}

class BottomSheetController: TitleLabelController {
    
    private let search = UISearchBar()
    
    weak var delegate: BottomSheetDelegate?
    
//    private let dbService = DatabaseService()
    private var tableView: UITableView!
    var places: [PlaceDTO] = [] {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        titleText = "Рядом"
        view.roundCorners(corners: UIRectCorner([.topLeft, .topRight]), radius: 20)
    }
    
    private func initViews() {
        
        view.backgroundColor = .darkBgColor
        
        initTable()
        initSearch()
        initConstraints()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        search.resignFirstResponder()
    }
    
    private func initSearch() {
        
        search.searchBarStyle = .minimal
        search.translatesAutoresizingMaskIntoConstraints = false
        search.showsBookmarkButton = true
        search.setImage(#imageLiteral(resourceName: "filter"), for: .bookmark, state: .normal)
        search.placeholder = "Поиск"
        search.delegate = self
        definesPresentationContext = true
        view.addSubview(search)
    }
    
    private func initTable() {
        
        tableView = UITableView().enableConstraints()
        tableView.backgroundColor = .darkBgColor
        tableView.register(cell: PlaceCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        tableView.separatorColor = #colorLiteral(red: 0.4588235294, green: 0.4745098039, blue: 0.5568627451, alpha: 0.6)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 22, bottom: 0, right: 23)
        
        view.addSubview(tableView)
    }
            
    private func initConstraints() {
        
        NSLayoutConstraint.activate([
            
            search.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 10),
            search.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10),
            search.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10),
            
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 22),
            titleLabel.topAnchor.constraint(equalTo: search.bottomAnchor),
            
            tableView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

extension BottomSheetController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell: PlaceCell = tableView.dequeueTypedCell(for: indexPath)
        
        cell.configure(with: places[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedPlace = places[indexPath.row]
        
        let detailVC = PlaceDetailController()
        detailVC.place = selectedPlace
        detailVC.addCloseButton()
        
        let navVC = UINavigationController(rootViewController: detailVC)
        navVC.modalPresentationStyle = .fullScreen
        
        present(navVC, animated: true)
    }
}

extension BottomSheetController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        search.showsCancelButton = true
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        
        let filterVC = FilterViewController()
        let navVC = UINavigationController(rootViewController: filterVC)
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search.showsCancelButton = false
        search.text = ""
        search.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        guard let searchText = searchBar.text else { return }
        
        search.resignFirstResponder()
        delegate?.searchButtonPressed(with: searchText)
    }
}
