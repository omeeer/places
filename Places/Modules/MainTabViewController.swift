//
//  MainTabViewController.swift
//  Places
//
//  Created by Amir on 28.02.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {
    
    private let mapVC = MainMapModuleConfigurator.configuredModule()
    private let bookmarksVC = BookmarksModuleConfigurator.configuredModule()
    private let settingVC = SettingsViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    private func initViews() {
        
        tabBar.tintColor = .orange
        tabBar.isTranslucent = false
        tabBar.barTintColor = #colorLiteral(red: 0.1411764706, green: 0.1490196078, blue: 0.2, alpha: 1)
        
        mapVC.tabBarItem = UITabBarItem(title: "Карта", image: #imageLiteral(resourceName: "map"), tag: 1)
        
        let navBookmarks = UINavigationController(rootViewController: bookmarksVC)
        navBookmarks.tabBarItem = UITabBarItem(title: "Избранное", image: #imageLiteral(resourceName: "bookmarkTab"), tag: 1)
        
        let navSettings = UINavigationController(rootViewController: settingVC)
        navSettings.tabBarItem = UITabBarItem(title: "Настройки", image: #imageLiteral(resourceName: "settings"), tag: 1)
        
        viewControllers =  [mapVC, navBookmarks, navSettings]
    }    
}
