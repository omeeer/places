//
//  BookmarksBookmarksPresenter.swift
//  Places
//
//  Created by omeeer78 on 25/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

protocol BookmarksModuleInput: class {
    
}

import Foundation

class BookmarksPresenter: BookmarksModuleInput, BookmarksViewOutput, BookmarksInteractorOutput {    
        
    weak var view: BookmarksViewInput!
    var interactor: BookmarksInteractorInput!
    var router: BookmarksRouterInput!
    var dataSource: BookmarkDataSourceInput!
    
    //MARK: - BookmarksViewOutput
    func viewIsReady() {
        
        interactor.fetchSavedPlaces()
        
        view.set(dataSource: dataSource)
        
        checkPlaces()
    }
    
    func delete(place: PlaceDTO) {
        interactor.deleteFromDatabase(place: place)
    }
    
    func didSelectItem(with index: Int) {
        router.navigateToDetail(with: dataSource.places[index])        
    }
    
    //MARK: - BookmarksInteractorOutput
    func didFetchSaved(places: [PlaceDTO]) {        
        update(with: places)
        view.reloadCollection()
    }
    
    func didFinishFetching(places: [PlaceDTO]) {
        
        update(with: places)
        view.reloadCollection()
    }
    
    func didFinishDeletingPlace(with newArray: [PlaceDTO]) {
          
        DispatchQueue.main.async {
            self.update(with: newArray)
            self.view.reloadCollection()
        }
    }
    
    func didCheckPlacesIsEmpty(with status: Bool) {        
        status ? view.configureEmptyBookmarks() : view.configureCollectionView()
    }
    
    //MARK: - Private
    private func update(with places: [PlaceDTO]) {
        
        dataSource.places = places
        
        checkPlaces()
    }
    
    private func checkPlaces() {
        interactor.isPlacesEmpty()
    }
}
