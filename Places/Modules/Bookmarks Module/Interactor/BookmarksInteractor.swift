//
//  BookmarksBookmarksInteractor.swift
//  Places
//
//  Created by omeeer78 on 25/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

protocol BookmarksInteractorInput {
    
    func fetchSavedPlaces()
    func isPlacesEmpty()    
    func fetchBookmarks()
    func deleteFromDatabase(place: PlaceDTO)
}

protocol BookmarksInteractorOutput: class {
    
    func didFetchSaved(places: [PlaceDTO])
    func didCheckPlacesIsEmpty(with status: Bool)
    
    func didFinishFetching(places: [PlaceDTO])
    func didFinishDeletingPlace(with newArray: [PlaceDTO])
}

class BookmarksInteractor: BookmarksInteractorInput {
                
    weak var output: BookmarksInteractorOutput!
    var databaseService: DatabaseServiceProtocol!
    
    //MARK: - BookmarksInteractorInput
    func fetchSavedPlaces() {
        output.didFetchSaved(places: databaseService.getPlaces())
    }
    
    func isPlacesEmpty() {
        output.didCheckPlacesIsEmpty(with: databaseService.isPlacesEmpty())
    }
    
    //TODO: - DB logic
    func fetchBookmarks() {
        let places = databaseService.getPlaces()
        output.didFinishFetching(places: places)
    }
    
    func deleteFromDatabase(place: PlaceDTO) {
        
        databaseService.delete(place: place) { places in
            self.output.didFinishDeletingPlace(with: places)
        }
    }
}
