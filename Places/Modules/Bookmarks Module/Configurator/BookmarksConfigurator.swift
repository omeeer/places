//
//  BookmarksBookmarksConfigurator.swift
//  Places
//
//  Created by omeeer78 on 25/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import UIKit

class BookmarksModuleConfigurator {
    
    class func configuredModule() -> UIViewController {
        
        let viewController = BookmarksViewController()
        let interactor = BookmarksInteractor()
        let presenter = BookmarksPresenter()
        let router = BookmarksRouter()
        let dataSource = BookmarkDataSource()
//        let savedPlacesService = BookmarksService()
        let databaseService = DatabaseService()
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSource = dataSource
        
        interactor.output = presenter
//        interactor.bookmarksService = savedPlacesService
        interactor.databaseService = databaseService
                
        viewController.output = presenter
        
        dataSource.output = viewController
        
        router.viewController = viewController
        
        return viewController
    }
}
