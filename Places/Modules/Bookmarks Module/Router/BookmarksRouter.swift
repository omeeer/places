//
//  BookmarksBookmarksRouter.swift
//  Places
//
//  Created by omeeer78 on 25/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import UIKit

protocol BookmarksRouterInput {
    func navigateToDetail(with place: PlaceDTO)
}

class BookmarksRouter: BookmarksRouterInput {
    
    var viewController: UIViewController!
    
    func navigateToDetail(with place: PlaceDTO) {
        
        let placeDetailVC = PlaceDetailController()
        placeDetailVC.place = place
        
        viewController.navigationItem.backBarButtonItem?.title = ""
        viewController.navigationController?.pushViewController(placeDetailVC, animated: true)
    }
}
