//
//  BookmarkDataSource.swift
//  Places
//
//  Created by Amir on 25.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol BookmarkDataSourceInput: UICollectionViewDataSource {
    var places: [PlaceDTO] { get set }
}

protocol BookmarkDataSourceOutput: SavedPlaceCellDelegate {
    
}

class BookmarkDataSource: NSObject, BookmarkDataSourceInput {
    
    var places: [PlaceDTO] = []
    
    var output: BookmarkDataSourceOutput!
    
    //MARK: - BookmarkDataSourceInput
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return places.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: SavedPlaceCell = collectionView.dequeueTypedCell(for: indexPath)
        
        cell.configure(with: places[indexPath.row], delegate: output)
        
        return cell
    }
}
