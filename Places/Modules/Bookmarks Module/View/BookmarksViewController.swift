//
//  BookmarksBookmarksViewController.swift
//  Places
//
//  Created by omeeer78 on 25/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import UIKit

protocol BookmarksViewInput: class {

    func set(dataSource: UICollectionViewDataSource)
    func reloadCollection()
    func configureEmptyBookmarks()
    func configureCollectionView()
    func deleteItem(at index: Int)
}

protocol BookmarksViewOutput {
    
    func viewIsReady()
    func delete(place: PlaceDTO)
    func didSelectItem(with index: Int)
}

class BookmarksViewController: UIViewController, BookmarksViewInput {
    
    var output: BookmarksViewOutput!
    
    private lazy var bookmarksCollectionView = BookmarksCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Избранное"
        view.backgroundColor = .darkBgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        output.viewIsReady()
        
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func configureCollectionView() {
        
        view.subviews.forEach { $0.removeFromSuperview() }
        
        bookmarksCollectionView.customDelegate = self
        view = bookmarksCollectionView
    }
    
    func configureEmptyBookmarks() {
        
        view.subviews.forEach { $0.removeFromSuperview() }
        view.addSubview(EmptyBookmarksView(frame: view.bounds))
    }
    
    //MARK: - BookmarksViewInput
    func set(dataSource: UICollectionViewDataSource) {
        bookmarksCollectionView.dataSource = dataSource
    }
        
    func deleteItem(at index: Int) {
        
        let indexPath = IndexPath(item: index, section: 0)
                
        bookmarksCollectionView.deleteItems(at: [indexPath])
                
        DispatchQueue.main.async {
            self.reloadCollection()
        }
    }
    
    func reloadCollection() {
        bookmarksCollectionView.reloadData()
    }
}

extension BookmarksViewController: BookmarksCollectionViewDelegate {
    
    func didSelectItem(at indexPath: IndexPath) {
        output.didSelectItem(with: indexPath.row)
    }
}

//MARK: - BookmarksPresenter extension
extension BookmarksViewController: BookmarkDataSourceOutput {
    
    func bookmarkButtonPressed(with place: PlaceDTO) {
        output.delete(place: place)
    }
}
