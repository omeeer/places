//
//  BookmarksCollectionView.swift
//  Places
//
//  Created by Amir on 26.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol BookmarksCollectionViewDelegate: AnyObject {
    func didSelectItem(at indexPath: IndexPath)
}

class TagsLayout: UICollectionViewFlowLayout {

    required override init() {
        super.init()
        common()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        common()
    }

    private func common() {
        estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        minimumLineSpacing = 10
        minimumInteritemSpacing = 30
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {

        guard let att = super.layoutAttributesForElements(in:rect) else { return [] }
        
        var x: CGFloat = sectionInset.left
        var y: CGFloat = -1.0

        for a in att {
            if a.representedElementCategory != .cell { continue }

            if a.frame.origin.y >= y { x = sectionInset.left }
            a.frame.origin.x = x
            x += a.frame.width + minimumInteritemSpacing
            y = a.frame.maxY
        }
        return att
    }
}

class BookmarksCollectionView: UICollectionView {
    
    weak var customDelegate: BookmarksCollectionViewDelegate?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        delegate = self
        
        register(cell: SavedPlaceCell.self)
        
        let layout = TagsLayout()
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.sectionInset = UIEdgeInsets(top: 20, left: 15, bottom: 0, right: 0)
        setCollectionViewLayout(layout, animated: false)
        
        backgroundColor = .darkBgColor
        
        //        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
        //          flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        //        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }    
}

extension BookmarksCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        customDelegate?.didSelectItem(at: indexPath)
    }
}
