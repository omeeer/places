//
//  EmptyBookmarksView.swift
//  Places
//
//  Created by Amir on 26.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class EmptyBookmarksView: UIView {
    
    private let imageView = UIImageView(image: #imageLiteral(resourceName: "box"))
    private let label = UILabel(textColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7), font: .blackFont(size: 24))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    private func initViews() {
        
        label.text = "Здесь ничего нет, добавьте свое первое место"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.sizeToFit()
        
        backgroundColor = .darkBgColor
        
        let mainStack = UIStackView(views: [imageView, label], axis: .vertical, spacing: 10, distribution: .fill).enableConstraints()
            
        addSubview(mainStack)
        
        NSLayoutConstraint.activate([
            
            imageView.heightAnchor.constraint(equalToConstant: 300),
            mainStack.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7),
            mainStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            mainStack.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 15)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
