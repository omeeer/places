//
//  PlaceDetailController.swift
//  Places
//
//  Created by Amir on 07.04.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit
import heresdk

class PlaceDetailController: AppViewController {
    
    private var initialContentOffset: CGFloat!
    private var placeDetailMainView: PlaceDetailMainView!
    private var locationService: LocationServiceProtocol!
    
    private let bookmarkButton = ButtonBuilder().build()
    
    private let dbService = DatabaseService()
    
    private var isSaved: Bool = Bool() {
        didSet {
            let image = isSaved ? #imageLiteral(resourceName: "cachedBookmark") : #imageLiteral(resourceName: "bookmark.png")
            bookmarkButton.setImage(image, for: .normal)
        }
    }
    
    var place: PlaceDTO! {
        didSet {
            isSaved = dbService.isPlaceSaved(place: place)
        }
    }
    
    override func loadView() {
        
        placeDetailMainView = PlaceDetailMainView()
        placeDetailMainView.configure(delegate: self, place: place!)
        
        view = placeDetailMainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationService = LocationService()
        view.backgroundColor = .darkBgColor
        setupNavigation()
        
        placeDetailMainView.setScrollView(delegate: self)
        
        initialContentOffset = placeDetailMainView.getScrollViewContentOffset().y
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
    }
    
    private func setupNavigation() {
        
        bookmarkButton.addTarget(self, action: #selector(bookmarkButtonPressed), for: .touchUpInside)
        let rightBtn = UIBarButtonItem(customView: bookmarkButton)
        navigationItem.rightBarButtonItem = rightBtn
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)        
        
        navigationController?.navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.heavyFont(size: 28)
        ]
    }
    
    @objc private func bookmarkButtonPressed() {
        dbService.isPlaceSaved(place: place) ? delete(place: place) : save(place: place)
    }
    
    private func delete(place: PlaceDTO) {
        
        dbService.deleteWithStatus(place: place) { (isDeleted) in
            DispatchQueue.main.async {
                if isDeleted {
                    self.isSaved.toggle()
                }
            }
        }                
    }
    
    private func save(place: PlaceDTO) {
        
        dbService.save(place: place) { isSuccess in
            DispatchQueue.main.async {
                if isSuccess {
                    self.isSaved.toggle()
                }
            }
        }
    }
    
}

extension PlaceDetailController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard let navHeight = navigationController?.navigationBar.frame.height else { return }
        
        if scrollView.contentOffset.y - (-initialContentOffset) >= navHeight / 2 {
            title = place?.name
            navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        } else {
            title = ""
        }
    }
}

extension PlaceDetailController: PlaceDetailMainViewDelegate {
    
    func directionButtonPressed() {
        
        let geoCoordinate = GeoCoordinates(latitude: place.coordinates.lat, longitude: place.coordinates.lng)
        
        NotificationCenter.default.post(name: .didReceiveRoute, object: geoCoordinate)
        
        guard let tabBar = tabBarController else {
            dismiss(animated: true)
            return
        }
        tabBar.selectedIndex = 0
    }
}
