//
//  PlaceDetailMainView.swift
//  Places
//
//  Created by Amir on 05.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

protocol PlaceDetailMainViewDelegate: AnyObject {
    func directionButtonPressed()
}

class PlaceDetailMainView: UIView {
    
    private var directionButton = ButtonBuilder()
        .backgroundColor(.appOrange)
        .cornerRadius(10)
        .title("Построить маршрут")
        .titleColor(.white)
        .font(.heavyFont(size: 15))
        .build()
    private let descriptionTable = DescriptionTableView().enableConstraints()
    private var placeHeader: PlaceInfoHeader!
    private let scrollView = UIScrollView().enableConstraints()
    
    weak var delegate: PlaceDetailMainViewDelegate?
    private var place: PlaceDTO?
    
    func configure(delegate: PlaceDetailMainViewDelegate, place: PlaceDTO) {
        
        self.place = place
        self.delegate = delegate
        
        configureTableView()
        configureHeaderView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setScrollView(delegate: UIScrollViewDelegate) {
        scrollView.delegate = delegate
    }
    
    func getScrollViewContentOffset() -> CGPoint {
        return scrollView.contentOffset
    }
    
    private func initViews() {
        
        setupDescriptionTable()
        setupDirectionButton()
        setupPlaceHeader()
        
        scrollView.contentInsetAdjustmentBehavior = .never
        addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        let stack = UIStackView(views: [placeHeader, descriptionTable, directionButton], axis: .vertical, spacing: 30, distribution: .equalSpacing).enableConstraints()
        stack.alignment = .center
        scrollView.addSubview(stack)
        
        NSLayoutConstraint.activate([
            
            descriptionTable.widthAnchor.constraint(equalTo: stack.widthAnchor),
            directionButton.widthAnchor.constraint(equalTo: stack.widthAnchor, multiplier: 0.6),
            
            stack.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stack.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -30),
            stack.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stack.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
    }
    
    private func setupDirectionButton() {
        
        directionButton.addTarget(self, action: #selector(directionButtonPressed), for: .touchUpInside)
        
        scrollView.addSubview(directionButton)
        
        NSLayoutConstraint.activate([
            directionButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    private func setupPlaceHeader() {
        
        placeHeader = PlaceInfoHeader().enableConstraints()
        
        scrollView.addSubview(placeHeader)
        
        NSLayoutConstraint.activate([
            placeHeader.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            placeHeader.heightAnchor.constraint(equalToConstant: 190)
        ])
    }
    
    private func setupDescriptionTable() {
        NSLayoutConstraint.activate([
            descriptionTable.heightAnchor.constraint(equalToConstant: 350)
        ])
    }
    
    private func configureHeaderView() {
        guard let selectedPlace = place else { return }
        placeHeader.configure(with: selectedPlace)
    }
    
    private func configureTableView() {
        descriptionTable.placeDTO = place
    }
    
    @objc private func directionButtonPressed() {
        delegate?.directionButtonPressed()
    }
}

