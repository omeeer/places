//
//  SettingsViewController.swift
//  Places
//
//  Created by Amir on 28.02.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class SettingsViewController: AppViewController {

    private let radiusSettingView = TitledTextField(title: "Радиус поиска мест (в метрах)", placeholder: "1000 по умолчанию").enableConstraints()
    private let saveButton = ButtonBuilder()
        .backgroundColor(.appOrange)
        .title("Сохранить")
        .cornerRadius(10)
        .font(.heavyFont(size: 15))
        .titleColor(.white)
        .build()
    private lazy var radiusStackView = UIStackView(views: [radiusSettingView, saveButton], axis: .vertical, spacing: 22, distribution: .fill).enableConstraints()
    
    private let userDefaultsService = UserDefaultsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Настройки"
        initViews()
    }
    
    private func initViews() {
        
        view.addSubview(radiusStackView)
        saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            
            saveButton.heightAnchor.constraint(equalToConstant: 50),
            
            radiusStackView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 16),
            radiusStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24),
            radiusStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24),
        ])
    }
    
    @objc private func saveButtonPressed() {
        
        guard let savedRadius = radiusSettingView.getTextFieldText(),
            let intValue = Int(savedRadius)
            else { return }
        
        UserDefaultsService().save(value: intValue, for: Constants.searchRadiusUserDefaultsKey)
        
        let alert = UIAlertController(title: "Успешно", message: "Новый радиус поиска мест: \(intValue)м.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .cancel))
        present(alert, animated: true)
        
    }
}
