//
//  MainMapMainMapRouter.swift
//  Places
//
//  Created by omeeer78 on 24/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import UIKit

protocol MainMapRouterInput {
    
    func presentPlaceDetail(with place: PlaceDTO)
    var vc: UIViewController! { get set }
    func showErrorAlert(title: String, message: String)
}

class MainMapRouter: MainMapRouterInput {
    
    var vc: UIViewController!
  
    func showErrorAlert(title: String, message: String) {
        
        let errorAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        errorAlert.addAction(UIAlertAction(title: "Понятно", style: .cancel))
        
        vc.present(errorAlert, animated: true)
    }
    
    func presentPlaceDetail(with place: PlaceDTO) {
        
        let detailVC = PlaceDetailController()
        detailVC.place = place
        detailVC.addCloseButton()
        
        let navVC = UINavigationController(rootViewController: detailVC)
        navVC.modalPresentationStyle = .fullScreen
        
        vc.present(navVC, animated: true)
    }    
}
