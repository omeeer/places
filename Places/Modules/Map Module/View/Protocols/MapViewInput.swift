//
//  MapViewInput.swift
//  Places
//
//  Created by Amir on 27.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import heresdk

protocol MapViewInput: class {
    
    func centerMapOn(coordinates: GeoCoordinates)
    func addMarkers(to places: [PlaceDTO])
    func didGetRoute(mapRoute: MapRoute)
}
