//
//  MapViewOutput.swift
//  Places
//
//  Created by Amir on 27.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import heresdk

protocol MapViewOutput {
    
    func viewIsReady()
    func searchForCategory(categories: [String])    
    func locationButtonPressed()
    func didSelect(place: PlaceDTO)
    func searchPlace(with text: String)
    
    func getRoute(by coordinates: GeoCoordinates, type: DirectionType)
}
