//
//  MapView.swift
//  Places
//
//  Created by Amir on 27.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit
import heresdk

protocol MapViewDelegate: AnyObject {
    func didSelect(place: PlaceDTO)        
}

class MapView: UIView {
    
    private var mapView: MapViewLite!
    
    weak var delegate: MapViewDelegate?
    
    private var placeDTOs = [PlaceDTO]()
    
    private var markers = [MapMarkerLite]()
    private var polylines = [MapPolylineLite]()
    private var userLocationMarkers = [MapMarkerLite]()
    
    private var isOverlayShown = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initViews()
    }
    
    private func initViews() {
        
        mapView = MapViewLite(frame: bounds)
        mapView.mapScene.loadScene(mapStyle: .normalDay, callback: nil)
        mapView.gestures.tapDelegate = self
        
        addSubview(mapView)
    }
    
    func set(places: [PlaceDTO]) {
        placeDTOs = places
    }
    
    func centerMapOn(coordinates: GeoCoordinates) {
        
        userLocationMarkers.forEach { mapView.mapScene.removeMapMarker($0) }
        userLocationMarkers.removeAll()
        
        let mapMarker = MapMarkerLite(at: coordinates)
        
        let mapMarkerImageStyle = MapMarkerImageStyleLite()
        mapMarkerImageStyle.setAnchorPoint(Anchor2D(horizontal: 0.5, vertical: 1))
        
        mapMarker.addImage(MapImageLite(#imageLiteral(resourceName: "userArrow"))!, style: mapMarkerImageStyle)
        
        userLocationMarkers.append(mapMarker)
        
        mapView.mapScene.addMapMarker(mapMarker)
        mapView.camera.setTarget(coordinates)
        mapView.camera.setZoomLevel(17)
    }
    
    func locationButtonPressed() {
        DispatchQueue.main.async {
            self.clearMap()            
        }
    }
    
    func addMarkers(with places: [PlaceDTO]) {
        
        clearMap()
        
        guard let mapImage = MapImageLite(#imageLiteral(resourceName: "place")) else { return }
        let mapMarkerImageStyle = MapMarkerImageStyleLite()
        mapMarkerImageStyle.setScale(0.2)
        
        places.forEach { [weak self] place in
            
            let geoCoordinate = GeoCoordinates(latitude: place.coordinates.lat, longitude: place.coordinates.lng)
            let mapMarker = MapMarkerLite(at: geoCoordinate)
            mapMarker.addImage(mapImage, style: mapMarkerImageStyle)
            
            self?.markers.append(mapMarker)
        }
        
        markers.forEach { self.mapView.mapScene.addMapMarker($0) }
    }
    
    func clearMap() {
                        
        markers.forEach { mapView.mapScene.removeMapMarker($0) }
        polylines.forEach { mapView.mapScene.removeMapPolyline($0) }
        
        markers.removeAll()
        polylines.removeAll()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func handleOverlay(with place: PlaceDTO, coordinates: GeoCoordinates) {
        
        if !isOverlayShown {
            
            let overlay = MapOverlay(place: place)
            overlay.delegate = self
            
            overlay.layer.anchorPoint = CGPoint(x: 0.5, y: 1.5)
            
            let mapOverlay = MapOverlayLite(view: overlay, geoCoordinates: coordinates)
            mapView.addMapOverlay(mapOverlay)
            
            isOverlayShown.toggle()
        } else {
            mapView.overlays.forEach { mapView.removeMapOverlay($0) }
            isOverlayShown.toggle()
        }
    }
    
    func showRouteOnMap(mapRoute: MapRoute) {
        
        let routeGeoPolyline = try! GeoPolyline(vertices: mapRoute.route.polyline)
        let mapPolylineStyle = MapPolylineStyleLite()             
        
        mapPolylineStyle.setColor(0x00908AA0, encoding: .rgba8888)
        mapPolylineStyle.setWidthInPixels(inPixels: 10)
        let routeMapPolyline = MapPolylineLite(geometry: routeGeoPolyline, style: mapPolylineStyle)
        mapView.mapScene.addMapPolyline(routeMapPolyline)
        polylines.append(routeMapPolyline)
            
        addCircleMapMarker(geoCoordinates: mapRoute.initialCoordinates, image: #imageLiteral(resourceName: "userArrow"))
        addCircleMapMarker(geoCoordinates: mapRoute.destinaionCoordinates, image: #imageLiteral(resourceName: "destinationPoint"), scaleImage: true, scaleValue: 0.4)
    }
    
    private func addCircleMapMarker(geoCoordinates: GeoCoordinates, image: UIImage, scaleImage: Bool = false, scaleValue: Float = 0) {
        
        markers.forEach { mapView.mapScene.removeMapMarker($0) }
        markers.removeAll()
        
        let mapMarker = MapMarkerLite(at: geoCoordinates)
        let mapImage = MapImageLite(image)
        
        let mapMarkerImageStyle = MapMarkerImageStyleLite()
        if scaleImage {
         mapMarkerImageStyle.setScale(scaleValue)
        }
        mapMarker.addImage(mapImage!, style: mapMarkerImageStyle)
        mapView.mapScene.addMapMarker(mapMarker)
        markers.append(mapMarker)
    }
}

extension MapView: TapDelegate {
    
    func onTap(origin: Point2D) {
        
        mapView.overlays.forEach { mapView.removeMapOverlay($0) }
        
        mapView.pickMapItems(at: origin, radius: 2, completion: onMapItemsPicked)
    }
    
    func onMapItemsPicked(pickedMapItems: PickMapItemsResultLite?) {
        
        guard let topmostMapMarker = pickedMapItems?.topmostMarker else { return }
        
        let markerPositiion = Position(lat: topmostMapMarker.coordinates.latitude, lng: topmostMapMarker.coordinates.longitude)
        
        guard let index = placeDTOs.firstIndex(where: { $0.coordinates.lat == markerPositiion.lat && $0.coordinates.lng == markerPositiion.lng }) else { return }
        
        handleOverlay(with: placeDTOs[index], coordinates: topmostMapMarker.coordinates)
    }
}

extension MapView: MapOverlayDelegate {
    
    func didPressOnOverlay(with place: PlaceDTO) {
        
        mapView.overlays.forEach { mapView.removeMapOverlay($0) }
        
        delegate?.didSelect(place: place)
    }
}
