//
//  MapViewController.swift
//  Places
//
//  Created by omeeer78 on 24/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import FloatingPanel
import heresdk

class MapViewController: UIViewController, MapViewInput {
    
    var output: MapViewOutput!
    
    private let locationButton = ButtonBuilder()
        .image(#imageLiteral(resourceName: "clearMap"))
        .roundCorners()
        .backgroundColor(.appOrange)
        .build()
        .enableConstraints()
    private var fpc: FloatingPanelController!
    private var mapView = MapView()
    
    private let locationButtonSize = CGSize(width: 70, height: 70)
    private let rightOffset: CGFloat = 10
    private let bottomOffset: CGFloat = 130
    
    private let contentVC = BottomSheetController()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .didReceiveCategories, object: nil)
        NotificationCenter.default.removeObserver(self, name: .didReceiveRoute, object: nil)
    }
    
    override func loadView() {
        view = mapView
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
        mapView.delegate = self
        initViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didRecieveCategories(_ :)), name: .didReceiveCategories, object:  nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveRoute(_ :)), name: .didReceiveRoute, object:  nil)
    }
    
    private func initViews() {
        
        initMyLocationButton()
        initFloatingPanel()
    }
    
    private func initFloatingPanel() {
                        
        contentVC.delegate = self
        fpc = FloatingPanelController()
        fpc.surfaceView.backgroundColor = .clear
        fpc.delegate = self
        fpc.set(contentViewController: contentVC)
        fpc.addPanel(toParent: self)
    }
    
    private func initMyLocationButton() {
        
        view.addSubview(locationButton)
        locationButton.addTarget(self, action: #selector(locationButtonPressed), for: .touchUpInside)
        
        NSLayoutConstraint.activate([
            
            locationButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -rightOffset),
            locationButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottomOffset),
            locationButton.heightAnchor.constraint(equalToConstant: locationButtonSize.height),
            locationButton.widthAnchor.constraint(equalToConstant: locationButtonSize.width)
        ])
    }
    
    @objc private func locationButtonPressed() {        
        mapView.locationButtonPressed()
    }
    
    @objc private func didRecieveCategories(_ notification: Notification) {
        
        guard let categories = notification.object as? [String] else { return }
        output.searchForCategory(categories: categories)
    }
    
    @objc private func didReceiveRoute(_ notification: Notification) {
        
        guard let coordinates = notification.object as? GeoCoordinates else { return }
        
        output.getRoute(by: coordinates, type: .car)
    }
    
    func didGetRoute(mapRoute: MapRoute) {
        mapView.showRouteOnMap(mapRoute: mapRoute)
    }
    
    //MARK: - MapViewInput
    func addMarkers(to places: [PlaceDTO]) {
        
        contentVC.places = places
        mapView.set(places: places)
        mapView.addMarkers(with: places)
    }
    
    func centerMapOn(coordinates: GeoCoordinates) {
        mapView.centerMapOn(coordinates: coordinates)
    }
}

extension MapViewController: FloatingPanelControllerDelegate {
    
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return CustomFloatingPanelLayout()
    }
}

extension MapViewController: MapViewDelegate {
    
    func didSelect(place: PlaceDTO) {
        output.didSelect(place: place)
    }
}

extension MapViewController: BottomSheetDelegate {
    
    func searchButtonPressed(with text: String) {
        output.searchPlace(with: text)
    }
}
