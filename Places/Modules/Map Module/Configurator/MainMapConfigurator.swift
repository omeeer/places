//
//  MainMapMainMapConfigurator.swift
//  Places
//
//  Created by omeeer78 on 24/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import UIKit

class MainMapModuleConfigurator {
    
    class func configuredModule() -> UIViewController {
        
        let viewController = MapViewController()
        let presenter = MapPresenter()
        let interactor = MapInteractor()
        let router = MainMapRouter()
        let userDefaultsService = UserDefaultsService()
        let locationService = LocationService()
        
        presenter.view = viewController
        presenter.router = router
        
        interactor.output = presenter
        interactor.locationService = locationService
        interactor.userDefaultsService = userDefaultsService        
        
        router.vc = viewController

        presenter.interactor = interactor
        viewController.output = presenter
        
        return viewController        
    }
}
