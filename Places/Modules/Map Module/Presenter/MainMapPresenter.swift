//
//  MapPresenter.swift
//  Places
//
//  Created by omeeer78 on 24/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import heresdk

class MapPresenter: MapViewOutput, MapInteractorOutput {
    
    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MainMapRouterInput!
    
    //MARK: - MainMapViewOutput
    
    func viewIsReady() {
        interactor.getUserLocation()        
    }
    
    func locationButtonPressed() {
        interactor.getUserLocation()
    }
    
    func didSelect(place: PlaceDTO) {
        router.presentPlaceDetail(with: place)
    }
    
    func searchForCategory(categories: [String]) {
        interactor.searchPlaces(for: categories)
    }
    
    func searchPlace(with text: String) {
        interactor.searchPlaceBy(name: text)
    }
    
    func getRoute(by coordinates: GeoCoordinates, type: DirectionType) {
        interactor.getRoute(by: coordinates, type: type)
    }
    
    //MARK: - MainMapInteractorOutput    
    func didFind(places: [PlaceDTO], with type: SearchType) {
        
        if places.count == 0 {
            var errorText = "Не удалось найти места"
            let increaseSearchRadiusText = "попробуйте увеличить радиус поиска"
            
            switch type {
            case .category:
                errorText = "\(errorText) выбранной категории, \(increaseSearchRadiusText) или выбрать другие категрии"
            case .name:
                errorText = "\(errorText) с такой категорией или именем, \(increaseSearchRadiusText) или ввести другой запрос"
            }
            router.showErrorAlert(title: "Ошибка", message: errorText)
            return
        } else {
            view.addMarkers(to: places)
        }
    }
    
    func didGetRoute(mapRoute: MapRoute) {
        view.didGetRoute(mapRoute: mapRoute)
    }
    
}

//MARK: -  LocationServiceDelegate
extension MapPresenter: LocationServiceDelegate {
    
    func didUpdate(location: Location) {
        view.centerMapOn(coordinates: location.coordinates)
    }
}
