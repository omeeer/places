//
//  MainMapMainMapInteractor.swift
//  Places
//
//  Created by omeeer78 on 24/05/2020.
//  Copyright © 2020 ITIS. All rights reserved.
//

import heresdk

enum SearchType {
    case category, name
}

enum DirectionType {
    case pedestrian, car
}

protocol MapInteractorOutput: LocationServiceDelegate {
    func didFind(places: [PlaceDTO], with type: SearchType)
    func didGetRoute(mapRoute: MapRoute)
}

protocol MapInteractorInput {
        
    func getUserLocation()
    func searchPlaceBy(name: String)
    func searchPlaces(for categories: [String])
    func getRoute(by coordinates: GeoCoordinates, type: DirectionType)
}

class MapInteractor: MapInteractorInput {
            
    weak var output: MapInteractorOutput!
    var userDefaultsService: UserDefaultsServiceProtocol!
    var locationService: LocationServiceProtocol!
    
    func searchPlaces(for categories: [String]) {
        
        locationService.searchPlacesFor(categories: categories) { [weak self] (places, type) in
            self?.output.didFind(places: places, with: type)
        }
    }
    
    func searchPlaceBy(name: String) {
        
        locationService.searchPlaces(by: name) { [weak self] (places, type) in
            self?.output.didFind(places: places, with: type)
        }
    }
    
    func getUserLocation() {
        
        locationService.startLocating()
        locationService.delegate = output
    }
    
    func getRoute(by coordinates: GeoCoordinates, type: DirectionType) {
        
        locationService.getDirection(type: type, to: coordinates) { (route, initialCoordinate, destinationCoordinate) in
            let mapRoute = MapRoute(route: route, initialCoordinates: initialCoordinate, destinaionCoordinates: destinationCoordinate)
                        
            self.output.didGetRoute(mapRoute: mapRoute)
        }
    }
}
