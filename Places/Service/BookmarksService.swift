////
////  SavedPlacesService.swift
////  Places
////
////  Created by Amir on 25.05.2020.
////  Copyright © 2020 Amir Mardanov. All rights reserved.
////
//
//import Foundation
//
//protocol BookmarksServiceProtocol {
//    
//    func fetchSavedPlaces() -> [PlaceDTO]
//    func save(place: PlaceDTO)
//    func delete(place: PlaceDTO, completion: ([PlaceDTO], Int) -> Void)
//    func isPlacesEmpty() -> Bool
//}
//
//class BookmarksService: BookmarksServiceProtocol {
//    
//    private var places = [PlaceDTO](repeating: PlaceDTO(name: "Starbucks",
//                                                        type: "Кофейня",
//                                                        image: "starbucks",
//                                                        distance: 1.34,
//                                                        address: "ул. Островского, 38, Казань, Россия",
//                                                        phone: "+7 843 66 224 23 45",
//                                                        site: "superpupersite.com",
//                                                        workTime: "10:00-18:00",
//                                                        coordinates: Position(lat: 55.751510, lng: 49.197060),
//                                                        identifier: "id1"), count: 12)
//    
//    func fetchSavedPlaces() -> [PlaceDTO] {
//        return places
//    }
//    
//    func save(place: PlaceDTO) {
//        places.append(place)
//    }
//    
//    func delete(place: PlaceDTO, completion: ([PlaceDTO], Int) -> Void) {
//        
//        guard let index = places.firstIndex(where: { $0.name == place.name }) else { return }
//        
//        places.remove(at: index)
//        
//        completion(places, index)        
//    }
//    
//    func isPlacesEmpty() -> Bool {
//        return places.isEmpty
//    }
//}
