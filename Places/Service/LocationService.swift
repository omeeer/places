//
//  LocationManager.swift
//  Places
//
//  Created by Amir on 22.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import CoreLocation
import heresdk

protocol LocationServiceProtocol: CLLocationManagerDelegate {
    
    func startLocating()
    func stopLocating()
    func searchPlacesFor(categories: [String], completion: @escaping ([PlaceDTO], SearchType) -> Void)
    var delegate: LocationServiceDelegate? { get set }
    func searchPlaces(by name: String, completion: @escaping ([PlaceDTO], SearchType) -> Void)
    func getDirection(type: DirectionType, to destinationCoordinates: GeoCoordinates, completion: @escaping (Route, GeoCoordinates, GeoCoordinates) -> Void)
}

protocol LocationServiceDelegate: AnyObject {
    func didUpdate(location: Location)
}

class LocationService: NSObject, LocationServiceProtocol {
    
    var delegate: LocationServiceDelegate?
    private let locationManager = CLLocationManager()
    private var userLocation: GeoCoordinates?
    
    private var searchEngine: SearchEngine!
    private var routingEngine: RoutingEngine!
    
    override init() {
        do {
            try searchEngine = SearchEngine()
            try routingEngine = RoutingEngine()
        } catch let engineInstantiationError {
            fatalError("Failed to initialize SearchEngine. Cause: \(engineInstantiationError)")
        }
    }
    
    func startLocating() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func stopLocating() {
        locationManager.stopUpdatingLocation()
    }
    
    private func onLocationUpdated(location: CLLocation) {
        delegate?.didUpdate(location: HereConverter.convertLocation(nativeLocation: location))
    }
    
    func searchPlacesFor(categories: [String], completion: @escaping ([PlaceDTO], SearchType) -> Void) {
        
        let placeCategories = categories.map { PlaceCategory(id: $0) }
        guard let location = userLocation else {
            completion([], .category)
            return
        }
        let categoryQuery = CategoryQuery(placeCategories, areaCenter: location)
        let searchOptions = SearchOptions(languageCode: .ruRu, maxItems: 100)
        
        _ = searchEngine.search(categoryQuery: categoryQuery, options: searchOptions) { (error, places) in
            
            if let searchError = error {
                print("Search Error: \(searchError)")
                return
            }
            
            DispatchQueue.main.async {
                completion(HereConverter().getDecodedPlaceDTOs(from: places!, categories: categories), .category)
            }
        }
    }
    
    func searchPlaces(by name: String, completion: @escaping ([PlaceDTO], SearchType) -> Void) {
        
        let radius = UserDefaultsService().getValue(for: Constants.searchRadiusUserDefaultsKey) as? Int32 ?? 1000
        let searchOptions = SearchOptions(languageCode: .ruRu, maxItems: 100)
        
        guard let location = userLocation else {
            completion([], .name)
            return
        }
        let textQuery = TextQuery(name, in: GeoCircle(center: location, radiusInMeters: Double(radius)))
        
        _ = searchEngine.search(textQuery: textQuery, options: searchOptions) { error, places in
            
            if error != nil {
                completion([], .name)
                return
            }
            
            DispatchQueue.main.async {
                completion(HereConverter().getDecodedPlaceDTOs(from: places!), .name)
            }
        }
    }
    
    func getDirection(type: DirectionType, to destinationCoordinates: GeoCoordinates, completion: @escaping (Route, GeoCoordinates, GeoCoordinates) -> Void) {
        
        let initialCoordinates = userLocation!
        
        let waypoints = [Waypoint(coordinates: initialCoordinates), Waypoint(coordinates: destinationCoordinates)]
        
        switch type {
        case .car:
            let carOptions = CarOptions()
            routingEngine.calculateRoute(with: waypoints, carOptions: carOptions) { (routingError, routes) in
                
                if let error = routingError {
                    print("Error while calculating: \(error)")
                    return
                }
                
                guard let route = routes?.first else { return }
                
                completion(route, initialCoordinates, destinationCoordinates)
            }
        case .pedestrian:
            let pedestrianOptions = PedestrianOptions()
            print(pedestrianOptions)
        }
    }
}

//MARK: - CLLocationManagerDelegate
extension LocationService {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if let error = error as? CLError, error.code == .denied {
            manager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let lastLocation = locations.last else { return }
        
        guard lastLocation.coordinate.latitude != userLocation?.latitude,
            lastLocation.coordinate.longitude != userLocation?.longitude
            else { return }
        
        userLocation = GeoCoordinates(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
        
        onLocationUpdated(location: lastLocation)
        
        locationManager.stopUpdatingLocation()
    }
}
