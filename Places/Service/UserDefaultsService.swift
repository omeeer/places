//
//  UserDefaultsService.swift
//  Places
//
//  Created by Amir on 24.05.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import Foundation

protocol UserDefaultsServiceProtocol {
    
    func save(value: Any, for key: String)
    func getValue(for key: String) -> Any?
}

class UserDefaultsService: UserDefaultsServiceProtocol {
   
    func save(value: Any, for key: String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func getValue(for key: String) -> Any? {
        return UserDefaults.standard.value(forKey: key)
    }
}
