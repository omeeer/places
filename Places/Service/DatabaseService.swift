//
//  DatabaseService.swift
//  Places
//
//  Created by Amir on 06.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import RealmSwift

protocol DatabaseServiceProtocol {
    
    func save(place: PlaceDTO, completion: @escaping (Bool) -> Void)
    func delete(place: PlaceDTO, completion: @escaping ([PlaceDTO]) -> Void)
    func deleteWithStatus(place: PlaceDTO, completion: @escaping (Bool) -> Void)
    func getPlaces() -> [PlaceDTO]
    func isPlacesEmpty() -> Bool
    func isPlaceSaved(place: PlaceDTO) -> Bool
}

class DatabaseService: DatabaseServiceProtocol {
    
    func delete(place: PlaceDTO, completion: @escaping ([PlaceDTO]) -> Void) {
        
        DispatchQueue.global(qos: .utility).async {
            
            let realm = try! Realm(configuration: .defaultConfiguration)
            guard let objectToDelete = realm.object(ofType: PlaceRealm.self, forPrimaryKey: place.toRealmModel().identifier) else { return }
            
            try? realm.write {
                realm.delete(objectToDelete)
            }
            completion(self.getPlaces())
        }
    }
    
    func deleteWithStatus(place: PlaceDTO, completion: @escaping (Bool) -> Void) {
        
        var isDeletingSuccess = false
        
        DispatchQueue.global(qos: .utility).async {
            
            let realm = try! Realm(configuration: .defaultConfiguration)
            guard let objectToDelete = realm.object(ofType: PlaceRealm.self, forPrimaryKey: place.toRealmModel().identifier) else { return }
            
            try? realm.write {
                realm.delete(objectToDelete)
            }
            
            isDeletingSuccess = true
                        
            completion(isDeletingSuccess)
        }
    }
    
    func getPlaces() -> [PlaceDTO] {
        
        let realm = try! Realm(configuration: .defaultConfiguration)
        let models = realm.objects(PlaceRealm.self).sorted(byKeyPath: #keyPath(PlaceRealm.identifier), ascending: false)
        
        return models.map { $0.convertToDTO() }
    }
    
    func save(place: PlaceDTO, completion: @escaping (Bool) -> Void) {
        
        var isSavingSuccess = false
        
        DispatchQueue.global(qos: .utility).async {
            
            let realm = try! Realm(configuration: .defaultConfiguration)
            
            if realm.object(ofType: PlaceRealm.self, forPrimaryKey: place.identifier) == nil {
                
                try! realm.write {
                    realm.add(place.toRealmModel())
                }
                
                isSavingSuccess = true
                
                completion(isSavingSuccess)
            }
        }
    }
    
    func isPlacesEmpty() -> Bool {
        return getPlaces().isEmpty
    }
    
    func isPlaceSaved(place: PlaceDTO) -> Bool {
        
        let realm = try! Realm(configuration: .defaultConfiguration)
        
        let object = realm.object(ofType: PlaceRealm.self, forPrimaryKey: place.identifier)
        
        return object != nil
    }
}
