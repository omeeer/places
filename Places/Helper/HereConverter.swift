//
//  HereConverter.swift
//  Places
//
//  Created by Amir on 06.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import heresdk
import CoreLocation

class HereConverter {
    
    static func convertLocation(nativeLocation: CLLocation) -> Location {
        
        let geoCoordinates = GeoCoordinates(latitude: nativeLocation.coordinate.latitude,
                                            longitude: nativeLocation.coordinate.longitude,
                                            altitude: nativeLocation.altitude)
        var location = Location(coordinates: geoCoordinates,
                                timestamp: nativeLocation.timestamp)
        location.bearingInDegrees = nativeLocation.course
        location.speedInMetersPerSecond = nativeLocation.speed
        location.horizontalAccuracyInMeters = nativeLocation.horizontalAccuracy
        location.verticalAccuracyInMeters = nativeLocation.verticalAccuracy
        
        return location
    }
    
    
    func getDecodedPlaceDTOs(from places: [Place], categories: [String]? = nil) -> [PlaceDTO] {
        
        let radius = UserDefaultsService().getValue(for: Constants.searchRadiusUserDefaultsKey) as? Int32 ?? 1000
        
        var placeDTOs = [PlaceDTO]()
        
        for place in places {
            
            guard let distance = place.distanceInMeters, distance <= radius else { continue }
            
            var categoryNameIdString = ""
            
            if categories != nil {
                guard let index = place.details.categories.firstIndex(where: { categories?.contains($0.id) == true }) else { continue }
                categoryNameIdString = place.details.categories[index].id
            } else {
                guard let category = place.details.categories.filter({ CategoryType(rawValue: $0.id) != nil }).first else { continue }
                categoryNameIdString = category.id
            }
            
            let categoryString = CategoryConverter.convertToString(from: categoryNameIdString)
            guard let categoryImage = CategoryConverter.convertToImage(from: categoryNameIdString) else { return [] }
            
            let placeAddress = "\(place.address.streetName), \(place.address.houseNumOrName), \(place.address.city), \(place.address.country)"
            
            
            var placeAddr = ""
            reverseGeocode(location: place.coordinates) { (placemark) in
                                                                
                    placeAddr = "\(placemark.thoroughfare ?? "Улица не указана"), \(placemark.subThoroughfare ?? "Номер дома не указан"), \(placemark.locality ?? "Город не указан"), \(placemark.country!)"
                
            }                        
            
            let phoneNumber = place.details.contacts.first?.landlinePhoneNumbers.first ?? "Нет телефона"
            let website = place.details.contacts.first?.websiteAddresses.first ?? "Нет сайта"
            
            let isLatitudeEqual = placeDTOs.contains(where: { $0.coordinates.lat == place.coordinates.latitude })
            let isLongitudeEqual = placeDTOs.contains(where: { $0.coordinates.lng == place.coordinates.longitude })
            
            let worktime = place.details.openingHours.first?.text.first ?? "Часы работы не предоставлены"
            
            if [isLatitudeEqual, isLongitudeEqual].contains(false) {
                
                placeDTOs.append(PlaceDTO(name: place.title,
                                          type: categoryString,
                                          image: categoryImage,
                                          distance: Double(distance),
                                          address: placeAddress,
                                          phone: phoneNumber,
                                          site: website,
                                          workTime: worktime,
                                          coordinates: Position(lat: place.coordinates.latitude,
                                                                lng: place.coordinates.longitude),
                                          identifier: place.id))
                
            }
        }
        
        return placeDTOs
    }
    
    private func reverseGeocode(location: GeoCoordinates, completion: @escaping (CLPlacemark) -> Void) {
        
        let clLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
                        
        CLGeocoder().reverseGeocodeLocation(clLocation) { (placemarksArray, error) in
            
            if let error = error {
                print(error)
                return
            }
            
            guard let placemarks = placemarksArray, placemarks.count > 0 else { return }
            let placemark = placemarks[0]
            
            DispatchQueue.main.async {
                completion(placemark)
            }
        }
    }
}
