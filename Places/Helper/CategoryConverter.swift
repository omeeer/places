//
//  CategoryConverter.swift
//  Places
//
//  Created by Amir on 03.06.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

class CategoryConverter {
    
    static func convertToString(from categoryString: String) -> String {
        
        var categoryName = "Неизвестная категория"
        
        guard let category = CategoryType(rawValue: categoryString) else { return categoryName }
        
        switch category {
        case .bar:
            categoryName = "Бар"
        case .coffeeHouse:
            categoryName = "Кофейня"
        case .clothing:
            categoryName = "Магазин одежды"
        case .restaurant:
            categoryName = "Кафе/Ресторан"
        case .pharmacy:
            categoryName = "Аптека"
        case .nightClub:
            categoryName = "Ночной клуб"
        case .fastFood:
            categoryName = "Фастфуд"
        case .barberShop:
            categoryName = "Барбершоп"
        case .hairSalon:
            categoryName = "Салон красоты"
        case .gym:
            categoryName = "Спорт зал"
        case .museum:
            categoryName = "Музей"
        case .bookstore:
            categoryName = "Книжный магазин"
        case .foodStore:
            categoryName = "Продуктовый магазин"
        case .cinema:
            categoryName = "Кинотеатр"
        case .petGoods:
            categoryName = "Товары для животных"
        case .karaoke:
            categoryName = "Караоке"
        }
        return categoryName
    }
    
    static func convertToImage(from categoryString: String) -> String? {
        
        guard let category = CategoryType(rawValue: categoryString) else { return nil }
        
        switch category {
        case .bar:
            return "barType"
        case .coffeeHouse:
            return "coffeeType"
        case .clothing:
            return "clothingType"
        case .restaurant:
            return "restaurantType"
        case .foodStore:
            return "foodStoreType"
        case .pharmacy:
            return "pharmacyType"
        case .nightClub:
            return "nightClubType"
        case .fastFood:
            return "burgerType"
        case .barberShop:
            return "barbershopType"
        case .hairSalon:
            return "hairSalonType"
        case .gym:
            return "workoutType"
        case .museum:
            return "museumType"
        case .bookstore:
            return "bookstoreType"
        case .cinema:
            return "cinemaType"
        case .petGoods:
            return "petShopType"
        case .karaoke:
            return "karaokeType"
        }
        
    }
    
}
