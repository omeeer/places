//
//  AppDelegate.swift
//  Places
//
//  Created by Amir on 25.02.2020.
//  Copyright © 2020 Amir Mardanov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {                
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootVC = UserDefaultsService().getValue(for: Constants.onboardingUserDefaultsKey) == nil ? OnboardingViewController() : MainTabViewController()
                
        window?.rootViewController = rootVC
        window?.makeKeyAndVisible()                
        
        return true
    }
}

